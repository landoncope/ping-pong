# ping-pong

An unglamorous single-endpoint permanent forwarding web service written in Rust (rocket.rs).

## Running instructions

1. Install rust (tested with version `1.39.0-nightly (6ef275e6c 2019-09-24)`).
2. `ROCKET_TARGET_URL=https://teh-url-to-permanently-forward-to.com cargo run`