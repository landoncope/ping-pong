#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;

use rocket::response::Redirect;
use rocket::fairing::AdHoc;
use rocket::State;
use rocket::http::Status;

struct Forwarder {
    pub target_url: String
}

#[get("/health")]
fn health() -> Status {
    Status::Ok
}

#[get("/")]
fn index(sender: State<Forwarder>) -> Redirect {
    Redirect::permanent(sender.target_url.clone())
}

fn main() {
    rocket::ignite()
            .attach(AdHoc::on_attach("get-target-url", |r| {
                let target_url = r.config().get_str("target_url").unwrap().to_string();
                let forwarder = Forwarder {target_url: target_url};
                Ok(r.manage(forwarder))
            }))
            .mount("/", routes![index, health])
            .launch();
}